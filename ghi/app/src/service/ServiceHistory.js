import React, { useState, useEffect } from 'react';

function formatDateTime(dateTimeString) {
    const dateTime = new Date(dateTimeString);
    const date = dateTime.toLocaleDateString('en-US');
    const time = dateTime.toLocaleTimeString('en-US');
    return { date, time };
}

function ServiceHistory() {
    const [searchVin, setSearchVin] = useState('');
    const [appointments, setAppointments] = useState([]);
    const [filteredAppointments, setFilteredAppointments] = useState([]);

    const handleSearchChange = (event) => {
        setSearchVin(event.target.value);
    };

    const handleSearchSubmit = (event) => {
        event.preventDefault();
        filterAppointments();
    };

    const filterAppointments = () => {
        const filtered = appointments.filter(appointment =>
            appointment.vin.includes(searchVin)
        );
        setFilteredAppointments(filtered);
    };

    useEffect(() => {
        const fetchAppointments = async () => {
            try {
                const response = await fetch('http://localhost:8080/api/appointments/');
                if (response.ok) {
                    const data = await response.json();
                    setAppointments(data.appointments);
                }
            } catch (error) {
                console.error('Error fetching appointments:', error);
            }
        };

        fetchAppointments();
    }, []);

    return (
        <div>
            <h2>Appointment Search</h2>
            <form onSubmit={handleSearchSubmit}>
                <div className="mb-3">
                    <label htmlFor="searchVin" className="form-label">Search by VIN:</label>
                    <input type="text" className="form-control" id="searchVin" value={searchVin} onChange={handleSearchChange} />
                </div>
                <button type="submit" className="btn btn-primary">Search</button>
            </form>
            <div className="mt-4">
                <h3>Filtered Appointments:</h3>
                {filteredAppointments.length === 0 ? (
                    <p>No appointments found</p>
                ) : (
                    <table className="table">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>VIP</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredAppointments.map(appointment => {
                                const { date, time } = formatDateTime(appointment.date_time);
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.vip ? 'Yes' : 'No'}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{date}</td>
                                        <td>{time}</td>
                                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                )}
            </div>
        </div>
    );
}

export default ServiceHistory;
