import { NavLink } from 'react-router-dom';

function Nav() {

  return (
    <nav className="navbar navbar-dark bg-dark">
      <div className="container-fluid">
        <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar"
          aria-controls="offcanvasDarkNavbar" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <NavLink className="navbar-brand" to="/">WheelMaster</NavLink>

        <div className="offcanvas offcanvas-start text-bg-dark" tabIndex="-1" id="offcanvasDarkNavbar"
          aria-labelledby="offcanvasDarkNavbarLabel">
          <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasDarkNavbarLabel">Navigation</h5>
            <button type="button" className="btn-close btn-close-white" data-bs-dismiss="offcanvas"
              aria-label="Close"></button>
          </div>
          <div className="offcanvas-body">
            <ul className="navbar-nav justify-content-end flex-grow-1">
              <li className="nav-item" data-bs-dismiss="offcanvas">
                <NavLink className="nav-link" aria-current="page" to="/" >Home</NavLink>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Inventory
                </a>
                <ul className="dropdown-menu dropdown-menu-dark">
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="manufacturers/">List Manufacturers</NavLink></li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="manufacturers/create/">Create Manufacturer</NavLink></li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="models/">List Models</NavLink></li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="models/create/">Create Models</NavLink></li>
                  <li><hr className="dropdown-divider" /></li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="inventory/">List Inventory</NavLink></li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="inventory/create/">Create Inventory</NavLink></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Service
                </a>
                <ul className="dropdown-menu dropdown-menu-dark">
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="technicians/">List Technicians</NavLink></li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="technicians/create/">Create Technicians</NavLink></li>
                  <li data-bs-dismiss="offcanvas"><hr className="dropdown-divider" /></li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="appointments/">Current Appointments</NavLink></li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="appointments/history/">Historical Appointments</NavLink></li>
                  <li data-bs-dismiss="offcanvas"><NavLink className="dropdown-item" to="appointments/create/">Create Appointment</NavLink></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

  )
}

export default Nav;
