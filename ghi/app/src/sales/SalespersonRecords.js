import React, { useEffect, useState } from "react";

function SalespersonRecords() {
  const [salesperson, setSalesperson] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("");

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSelectedSalesperson(value);
  };

  const fetchSalespersonHistory = async () => {
    try {
      const salespersonUrl = "http://localhost:8090/api/sales/";
      const response = await fetch(salespersonUrl);
      if (!response.ok) {
        throw new Error("Couldn't fetch salesperson history");
      } else {
        const salespersonData = await response.json();
        console.log(salespersonData)
        setSalesperson(salespersonData.salespeople);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchSalespersonHistory();
  }, []);

  return (
    <>
      <h1>Salespersons' Records</h1>
      <div>
        <select
          onChange={handleSalespersonChange}
          value={selectedSalesperson}
          required
          id="salesperson"
          name="salesperson"
          className="form-select"
        >
          <option value="">Choose a salesperson</option>
          {salesperson.map((salesperson) => {
            return (
              <option
                key={salesperson.employee_id}
                value={salesperson.employee_id}
              >
                {salesperson.first_name} {salesperson.last_name}
              </option>
            );
          })}
        </select>
      </div>

      <table className="table table-success table-striped table-hover">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => {
            if (
              selectedSalesperson &&
              sale.salesperson.employee_id !== selectedSalesperson
            ) {
              return null;
            }
            else return (
              <tr key={sale.id}>
                <td>{sale.salesperson.first_name}</td>
                <td>{sale.customer.first_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
export default SalespersonRecords;
