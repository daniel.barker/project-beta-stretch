import React, { useState, useEffect } from "react";

function SaleAddForm() {
  const [automobile, setAutomobile] = useState("");
  const [salesperson, setSalesperson] = useState("");
  const [customer, setCustomer] = useState("");
  const [price, setPrice] = useState("");
  const [formSubmitted, setFormSubmitted] = useState(false);

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  };
  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };
  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.automobile = automobile;
    data.salesperson = salesperson;
    data.customer = customer;
    data.price = price;

    const saleUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = fetch(saleUrl, fetchConfig);
    if (response.ok) {
      const newSale = response.json();
      setAutomobile("");
      setSalesperson("");
      setCustomer("");
      setPrice("");
      setFormSubmitted(true);
    }
  }
    useEffect(() => {

    }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>New Sale</h1>
            <form
              onSubmit={handleSubmit}
              id="create-customer-form"
            >
              <div className="form-floating mb-3">
                <input
                  onChange={handleAutomobileChange}
                  value={automobile}
                  placeholder="automobile"
                  required
                  type="text"
                  automobile="automobile"
                  id="automobile"
                  className="form-control"
                />
                <label htmlFor="automobile">Automobile</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleSalespersonChange}
                  value={salesperson}
                  placeholder="salesperson"
                  required
                  type="text"
                  name="salesperson"
                  id="salesperson"
                  className="form-control"
                />
                <label htmlFor="salesperson">Salesperson</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleCustomerChange}
                  value={customer}
                  placeholder="customer"
                  required
                  type="text"
                  customer="customer"
                  id="customer"
                  className="form-control"
                />
                <label htmlFor="customer">Customer</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handlePriceChange}
                  value={price}
                  placeholder="price"
                  required
                  type="text"
                  price="price"
                  id="price"
                  className="form-control"
                />
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-success">Create</button>
            </form>
              New Sale Added
            </div>
          </div>
        </div>
    );
  };
export default SaleAddForm;
