import React, { useEffect, useState } from "react";
function SalespersonList() {
  const [salespersons, setSalespersons] = useState([]);

  useEffect(() => {
    const fetchSalespersons = async () => {
      try {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (!response.ok) {
          throw new Error("Failed to fetch salespersons");
        }
        const data = await response.json();
        setSalespersons(data.salespersons);
      } catch (error) {
        console.error(error);
      }
    };

    fetchSalespersons();
  }, []);
  return (
    <div>
      <h2>Salespeople</h2>
      <table className="table table-success table-striped table-hover">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespersons.map((salesperson) => {
            return (
              <tr key={salesperson.id}>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
                <td>{salesperson.employee_id}</td>
                <td>
                  {/* <button
                    type="button"
                    value={salesperson.id}
                    onClick={() => deleteSalesperson(salesperson.id)}
                  >
                    Delete Salesperson
                  </button> */}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default SalespersonList;
