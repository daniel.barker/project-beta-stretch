import React, { useEffect, useState } from "react";

function SaleList() {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    const fetchSales = async () => {
      try {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (!response.ok) {
          throw new Error("Failed to fetch sales");
        }
        const data = await response.json();
        setSales(data.sales);

      } catch (error) {
        console.error(error);
      }
    };

    fetchSales();
  }, []);
  return (
    <>
      <table className="table table-success table-striped table-hover">
        <thead>
          <tr>
            <th>Automobile</th>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.automobile.vin}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>${sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
export default SaleList;
